package Tools;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Hosein on 2/19/2016.
 */
public class YekanTextView extends TextView {
    public YekanTextView(Context context) {
        super(context);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "fonts/byekan.ttf");
        this.setTypeface(face);
    }

    public YekanTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "fonts/byekan.ttf");
        this.setTypeface(face);
    }

    public YekanTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "fonts/byekan.ttf");
        this.setTypeface(face);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public YekanTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "fonts/byekan.ttf");
        this.setTypeface(face);
    }
}

package Tools;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by hosein on 11/2/2015.
 */
public class Utils {

    Context _context;
    String Tag = "gym";
    public static String bank = "fanava";
    public static int mediaPerPage=6;
    public static String Domain="http://www.gymguide.ir/";
  // public static String Domain="http://192.168.1.17/";
   // public static String Domain="http://localhost:53076";
    public  Utils(){

    }

    public  Utils(Context mContext){
        _context=mContext;
    }

    public  static String GetImageUrl(String rowImageUrl)
    {
        if(rowImageUrl!=null && rowImageUrl.length()>1)
        {
            return rowImageUrl; //Domain + rowImageUrl.substring(1, rowImageUrl.length());
        }
        else
        {
            return  "";
        }
    }
    public  boolean isNetworkConnected() {
        NetworkInfo info = (NetworkInfo)((ConnectivityManager)_context
                .getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        if (info == null || !info.isConnected()) {
            return false;
        }
        if (info.isRoaming()) {
            // here is the roaming option you can change it if you want to
            // disable internet while roaming, just return false
            return false;
        }
        return true;
    }



    public boolean isOnline() {
        if (isNetworkConnected()) {
            System.out.println("executeCommand");
            Runtime runtime = Runtime.getRuntime();
            try
            {
                Process  mIpAddrProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
                int mExitValue = mIpAddrProcess.waitFor();
                System.out.println(" mExitValue "+mExitValue);
                if(mExitValue==0){
                    Domain="http://www.gymguide.ir/";
                    return true;
                }else{

                    Domain="http://192.168.1.17/";
                    return true;
                }
            }
            catch (InterruptedException ignore)
            {
                ignore.printStackTrace();
                System.out.println(" Exception:"+ignore);
            }
            catch (IOException e)
            {
                e.printStackTrace();
                System.out.println(" Exception:"+e);
            }
            Domain="http://192.168.1.17/";
            return true;
        }
        else {

            Toast.makeText(_context,"عدم اتصال به شبکه" ,Toast.LENGTH_LONG).show();
            return false;
        }
    }


    /**
     * Background Async Task to download file
     * */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a typical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment
                        .getDownloadCacheDirectory().toString());

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
          //  pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
          //  dismissDialog(progress_bar_type);

        }

    }
}

package Tools;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Hosein on 2/19/2016.
 */
public class SansTextView extends TextView {
    public SansTextView(Context context) {
        super(context);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "fonts/iran-sans.ttf");
        this.setTypeface(face);
    }

    public SansTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "fonts/iran-sans.ttf");
        this.setTypeface(face);
    }

    public SansTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "fonts/iran-sans.ttf");
        this.setTypeface(face);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SansTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "fonts/iran-sans.ttf");
        this.setTypeface(face);
    }
}

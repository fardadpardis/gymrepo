package Tools;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hosein on 12/2/2015.
 */
public class Session extends Application {


    // shared preferences
    SharedPreferences _pref;
    // editor for shared preferences
    SharedPreferences.Editor _editor;
    // Context for share preferences
    Context _context;
    // shared preferences mode
    int PRIVATE_MODE=0;
    //shared preferences name
    String PREF_NAME="mayan";

    public Session(Context context)
    {
        _context=context;
        _pref=_context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        _editor=_pref.edit();
    }
    public void SetSession(String key , String value)
    {
        _editor.putString(key , value);
        _editor.commit();
    }
    public void SetSession(String key , int value)
    {
        _editor.putInt(key , value);
        _editor.commit();
    }
    public void SetSession(String key , boolean value)
    {
        _editor.putBoolean(key , value);
        _editor.commit();
    }



    public String getSession(String key)
    {
        return _pref.getString(key,null);
    }

}

package Model;

/**
 * Created by hosein on 3/1/2016.
 */
public class Equipment {

    public int id ;
    public String name ;
    public String iconUrl ;
    public String description ;
    public String tag ;
    public Integer groupId ;
    public String code ;
    
}

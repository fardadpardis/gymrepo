package Model;

/**
 * Created by hosein on 3/1/2016.
 */
public class Document {
    public int id ;
    public String title ;
    public String description ;
    public String text ;
    public String date ;
    public Integer userId ;
    public String reference ;
    public String thumbUri ;
    public String coverUri ;
    public String tag ;
    public Integer likeCount ;
    public Integer viewCount ;
    public Integer groupId ;
    public String type ;
    public Integer priority ;
    public Boolean isActive ;
}

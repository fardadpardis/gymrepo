package Model;

import android.content.Context;
import android.view.View;

/**
 * Created by hosein on 3/1/2016.
 */
public class CustomMediaController extends android.widget.MediaController {

    public CustomMediaController(Context context, View anchor)
    {
        super(context);
        super.setAnchorView(anchor);
    }

    @Override
    public void setAnchorView(View view)
    {
        // Do nothing
    }
}

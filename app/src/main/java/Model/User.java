package Model;

/**
 * Created by hosein on 3/1/2016.
 */
public class User {
    public int userId;
    public String passWord;
    public String userName;
    public String role;
    public Boolean isActive;
    public String email;
    public String avatarUrl;
    public String mobile;
    public String coverUrl;
    public String registerDate;
}

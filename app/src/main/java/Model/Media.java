package Model;

/**
 * Created by hosein on 3/1/2016.
 */
public class Media {

    public int id ;
    public String name ;
    public String description;
    public String uri ;
    public String thumbUrl;
    public String tag ;
    public String type ;
    public int likeCount ;
    public int viewCount ;
    public int groupId ;
    public int equipmentId ;
    public boolean isLiked;
    public boolean isFaved;

}

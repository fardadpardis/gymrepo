package Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.hosein.Gym.R;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import java.util.List;

import Model.Document;
import Tools.SansTextView;
import Tools.Utils;
import Tools.YekanTextView;

/**
 * Created by hosein on 4/16/2016.
 */
public class ArticleListAdapter extends BaseAdapter {

    List<Document> articleslist;
    Context context;

    public ArticleListAdapter(Context context , List<Document> articleslist)
    {
        this.context=context;
        this.articleslist=articleslist;

    }
    @Override
    public int getCount() {
        return articleslist.size();
    }

    @Override
    public Object getItem(int position) {
        return articleslist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null)
        {
            LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.article_list_item,parent,false);
        }
        SansTextView brief = (SansTextView)convertView.findViewById(R.id.articleBrief);
        SansTextView title = (SansTextView)convertView.findViewById(R.id.articleTitle);
        YekanTextView author = (YekanTextView)convertView.findViewById(R.id.articleAuthor);
        YekanTextView date = (YekanTextView)convertView.findViewById(R.id.articleDate);

        final ImageView icon =(ImageView)convertView.findViewById(R.id.articleIcon);

        if(articleslist.get(position).description.length()>30)
        {
            brief.setText(articleslist.get(position).description.substring(0,30)+"...");
        }
        else
        {
            brief.setText(articleslist.get(position).description);
        }
        title.setText(articleslist.get(position).title);
        author.setText("ارسال شده توسط مدیریت");
        date.setText(articleslist.get(position).tag);

        UrlImageViewHelper.setUrlDrawable(icon, Utils.Domain + articleslist.get(position).thumbUri, R.drawable.cover, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String s, boolean b) {
                icon.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            }
        });
        convertView.setTag(articleslist.get(position).coverUri);
        return convertView;
    }
}

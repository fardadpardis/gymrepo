package Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.hosein.Gym.R;
import com.koushikdutta.async.Util;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import java.util.List;

import Model.Group;
import Tools.SansTextView;
import Tools.Utils;

/**
 * Created by hosein on 4/16/2016.
 */
public class ArticleGroupAdapter extends BaseAdapter {

    List<Group> articleGroups;
    Context context;

    public ArticleGroupAdapter(Context context , List<Group> articleGroups)
    {
        this.context=context;
        this.articleGroups=articleGroups;

    }
    @Override
    public int getCount() {
        return articleGroups.size();
    }

    @Override
    public Object getItem(int position) {
        return articleGroups.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null)
        {
            LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.article_group_item,parent,false);
        }
        SansTextView title = (SansTextView)convertView.findViewById(R.id.articleGroupTitle);
        final ImageView banner =(ImageView)convertView.findViewById(R.id.articleGroupBanner);

        title.setText(articleGroups.get(position).name);
        YoYo.with(Techniques.FadeInUp).playOn(title);
        UrlImageViewHelper.setUrlDrawable(banner, Utils.Domain + articleGroups.get(position).bannerUri, R.drawable.cover, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String s, boolean b) {
                banner.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        });

        convertView.setTag(articleGroups.get(position).id);
        return convertView;
    }
}

package Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hosein.Gym.R;

import java.util.List;

import NavigationDrawer.NavigationDrawerItem;
import Tools.SansTextView;


/**
 * Created by hosein on 11/16/2015.
 */
public class NavigationDrawerAdapter extends BaseAdapter
{
    viewHolder holder;
    Context context;
    List<NavigationDrawerItem> navItems;

    public NavigationDrawerAdapter(Context context , List<NavigationDrawerItem> navItems)
    {
        this.context=context;
        this.navItems=navItems;
    }

    @Override
    public int getCount() {
        return navItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null)
        {
            LayoutInflater _inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView= _inflater.inflate(R.layout.nav_menu_item,parent,false);
        }


        holder = new viewHolder();
        holder.title=(SansTextView)convertView.findViewById(R.id.navItemTitle);
        holder.tag=(SansTextView)convertView.findViewById(R.id.navItemStatus);
        holder.icon=(ImageView)convertView.findViewById(R.id.navItemIcon);
        holder.title.setText(navItems.get(position).title);
        //holder.tag.setText(navItems.get(position).tag);
        holder.icon.setImageResource(navItems.get(position).icon);



        convertView.setTag(navItems.get(position).tag);

        return convertView;
    }



    private class viewHolder
    {
        SansTextView tag , title;
        ImageView icon;
    }
}



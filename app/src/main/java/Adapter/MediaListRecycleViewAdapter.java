package Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.hosein.Gym.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.nineoldandroids.animation.Animator;

import java.util.List;

import Model.Media;
import Modules.MediaDetail;
import Tools.Message;
import Tools.Session;
import Tools.Utils;


/**
 * Created by hosein on 11/25/2015.
 */
public class MediaListRecycleViewAdapter extends RecyclerView.Adapter<MediaListRecycleViewAdapter.cartViewHolder> {

    public List<Media> mediaList;
    Context mContext;
    String done ="false";

    public MediaListRecycleViewAdapter(Context context, List<Media> mediaList)
    {
        this.mediaList=mediaList;
        mContext=context;
    }


    @Override
    public cartViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
        from(viewGroup.getContext()).
        inflate(R.layout.media_list_card,viewGroup,false);

        return new cartViewHolder(itemView, new cartViewHolder.IMyViewHolderClicks(){
            public void onItemSelect(String idp)
            {

            }
        });
    }
    @Override
    public void onBindViewHolder(final cartViewHolder cartHolder, int i) {
        final Media _media = mediaList.get(i);

        Log.d("gym faved", _media.isFaved + " media id " + _media.id);
        Log.d("gym liked", _media.isLiked + " media id " + _media.id);
        Log.d("gym", "-------------------------------------------------");


        UrlImageViewHelper.setUrlDrawable(cartHolder.mediaThumbnail, Utils.GetImageUrl(Utils.Domain+_media.thumbUrl),R.color.popular_media_bg, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String s, boolean b) {
                YoYo.with(Techniques.FadeIn).duration(500).playOn(imageView);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
        });
        if(_media.isFaved)
        {
            cartHolder.wishBtn.setImageResource(R.drawable.heart_active);
        }
        else
        {
            cartHolder.wishBtn.setImageResource(R.drawable.heart_passive);
        }
        if(_media.isLiked)
        {
            cartHolder.likeBtn.setImageResource(R.drawable.like_active);
        }
        else
        {
            cartHolder.likeBtn.setImageResource(R.drawable.like_passive);
        }
        cartHolder.wishBtn.setTag(_media.id);
        cartHolder.likeBtn.setTag(_media.id);
        cartHolder.downLoadBtn.setTag(_media.id);
        cartHolder.playIcon.setTag(_media.id);

        cartHolder.wishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView wishIcon = (ImageView) v;
                addToWishList(v.getTag().toString(),wishIcon);

            }
        });
        cartHolder.playIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loadMedia(_media.id);
            }
        });

        cartHolder.downLoadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView downloadIcon =(ImageView)v;

                String url = Utils.Domain+_media.uri;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                mContext.startActivity(i);

                downloadIcon.setImageResource(R.drawable.download_active);
                YoYo.with(Techniques.Flash).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        animation.start();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).playOn(cartHolder.downLoadBtn);
            }
        });

        cartHolder.likeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ImageView likeIcon = (ImageView) v;
                like(_media.id , likeIcon);

            }
        });
    }
    @Override
    public int getItemCount() {
        return mediaList.size();
    }

    public static class cartViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView mediaThumbnail , playIcon , wishBtn , likeBtn , downLoadBtn ;
        IMyViewHolderClicks  mListener;
        protected CardView card;
        public cartViewHolder( View v , IMyViewHolderClicks listener) {
            super(v);
            mListener=listener;
            this.mediaThumbnail=(ImageView)v.findViewById(R.id.mediaThumbnail);
            this.playIcon=(ImageView)v.findViewById(R.id.playBtn);
            this.wishBtn=(ImageView)v.findViewById(R.id.wishBtn);
            this.likeBtn = (ImageView)v.findViewById(R.id.likeBtn);
            this.downLoadBtn = (ImageView)v.findViewById(R.id.downLoadBtn);
            card = (CardView) v;
            v.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
          //  mListener.onItemSelect(v.getTag().toString());
        }


        public interface  IMyViewHolderClicks{
            public void onItemSelect(String idp);
        }
    }
    public void like(int mediaId , final ImageView likeIcon) {
        Session _session = new Session(mContext);
        Ion.with(mContext).load(Utils.Domain+"api/media/like")
                .setTimeout(30000)
                .setBodyParameter("userId",_session.getSession("userId"))
                .setBodyParameter("mediaId",mediaId+"")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            Log.d("gym",result.toString());
                            Gson gson = new Gson();
                            ImageView icon = likeIcon;
                            Message msg = gson.fromJson(result, Message.class);
                            if (msg.type == 0 && msg.text.equals("liked")) {
                                icon.setImageResource(R.drawable.like_active);
                                YoYo.with(Techniques.Tada).playOn(icon);

                            } else if (msg.type == 0 && msg.text.equals("disLiked")) {
                                icon.setImageResource(R.drawable.like_passive);
                            }
                        } catch (Exception exp) {
                            Log.d("cando", "Exception in like media");
                            done = "ابتدا وارد حساب کاربری خود شوید";
                        }
                    }
                });
    }


    public void addToWishList(String mediaId ,final ImageView wishIcon) {
        Session _session = new Session(mContext);
        Ion.with(mContext).load(Utils.Domain+"api/media/favorite")
                .setTimeout(30000)
                .setBodyParameter("userId",_session.getSession("userId"))
                .setBodyParameter("mediaId",mediaId)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            Log.d("gym",result.toString());
                            Gson gson = new Gson();
                            ImageView icon = wishIcon;
                            Message msg = gson.fromJson(result, Message.class);
                            if (msg.type == 0 && msg.text.equals("faved")) {

                                YoYo.with(Techniques.BounceIn).playOn(wishIcon);
                                wishIcon.setImageResource(R.drawable.heart_active);

                            } else if (msg.type == 0 && msg.text.equals("unfaved")) {
                                wishIcon.setImageResource(R.drawable.heart_passive);
                            }
                        } catch (Exception exp) {
                            Log.d("cando", "Exception in wish media");
                            done = "ابتدا وارد حساب کاربری خود شوید";
                        }
                    }
                });
    }


    public void loadMedia(final int mediaId)
    {
        Session session = new Session(mContext);
        Ion.with(mContext)
                .load(Utils.Domain + "/api/media/getMedia?mediaId=" + mediaId + "&userId=" + session.getSession("userId"))
                .setTimeout(30000)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {

                            FragmentManager fragmentManager = ((FragmentActivity) mContext).getSupportFragmentManager();
                            if(fragmentManager.findFragmentByTag("mediaDetail")==null) {
                                MediaDetail detailPage = MediaDetail.newInstance(result.toString());
                                fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right).add(R.id.MediaDetailContainer, detailPage, "mediaDetail").addToBackStack("mediaDetail").commit();
                            }
                            else
                            {
                                Fragment _target = fragmentManager.findFragmentByTag("mediaDetail");
                                fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right).remove(_target).commit();
                                MediaDetail detailPage = MediaDetail.newInstance(result.toString());
                                fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right).add(R.id.MediaDetailContainer, detailPage, "mediaDetail").addToBackStack("mediaDetail").commit();
                            }
                        }
                        catch (Exception exp)
                        {

                        }
                    }
                });
    }
}


package Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.hosein.Gym.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.nineoldandroids.animation.Animator;

import java.util.List;

import Model.Group;
import Model.Media;
import Modules.MediaDetail;
import Modules.MediaGroups;
import Tools.Session;
import Tools.Utils;
import Tools.YekanTextView;


/**
 * Created by hosein on 11/25/2015.
 */
public class MediaGroupRecycleViewAdapter extends RecyclerView.Adapter<MediaGroupRecycleViewAdapter.cartViewHolder> {

    public List<Group> groupList;
    Context mContext;
    String done ="false";

    public MediaGroupRecycleViewAdapter(Context context, List<Group> groupList)
    {
        this.groupList=groupList;
        mContext=context;
    }


    @Override
    public cartViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
        from(viewGroup.getContext()).
        inflate(R.layout.media_group, viewGroup, false);
        return new cartViewHolder(itemView, new cartViewHolder.IMyViewHolderClicks(){
            public void onItemSelect(String idp)
            {

            }
        });
    }
    @Override
    public void onBindViewHolder(final cartViewHolder cartHolder, int i) {
        final Group _group = groupList.get(i);
        if (_group.name.length() > 15) {
            cartHolder.groupTitle.setText(_group.name.substring(0, 15) + "...");
        }
        else
        {
            cartHolder.groupTitle.setText(_group.name);
        }

        UrlImageViewHelper.setUrlDrawable(cartHolder.groupIcon, Utils.GetImageUrl(Utils.Domain + _group.iconUri), R.color.popular_media_bg, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String s, boolean b) {
                YoYo.with(Techniques.FadeIn).duration(500).playOn(imageView);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
        });

        UrlImageViewHelper.setUrlDrawable(cartHolder.groupThumbnail, Utils.GetImageUrl(Utils.Domain + _group.bannerUri), R.color.popular_media_bg, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String s, boolean b) {
                YoYo.with(Techniques.FadeIn).duration(500).playOn(imageView);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        });



        cartHolder.card.setTag(_group.id);
        cartHolder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity) mContext).getSupportFragmentManager();
                if(fm.findFragmentByTag("subGroups")==null) {
                    MediaGroups detailPage = MediaGroups.newInstance(v.getTag().toString(),true);
                    fm.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right).add(R.id.MediaDetailContainer, detailPage, "subGroups").addToBackStack("subGroups").commit();
                }
                else
                {
                    Fragment _target = fm.findFragmentByTag("subGroups");
                    fm.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right).remove(_target).commit();
                    MediaGroups detailPage = MediaGroups.newInstance(v.getTag().toString(),true);
                    fm.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right).add(R.id.MediaDetailContainer, detailPage, "subGroups").addToBackStack("subGroups").commit();
                }
            }
        });

        cartHolder.groupIcon.setTag(_group.id);
        cartHolder.groupIcon.setTag(_group.id);

    }
    @Override
    public int getItemCount() {
        return groupList.size();
    }

    public static class cartViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView groupIcon ;
        public ImageView groupThumbnail ;
        public YekanTextView groupTitle;
        IMyViewHolderClicks  mListener;
        protected CardView card;
        public cartViewHolder( View v , IMyViewHolderClicks listener) {
            super(v);
            mListener=listener;
            this.groupIcon=(ImageView)v.findViewById(R.id.groupIcon);
            this.groupTitle=(YekanTextView)v.findViewById(R.id.groupTitle);
            this.groupThumbnail=(ImageView)v.findViewById(R.id.groupThumbnail);
            card = (CardView) v;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            //mListener.onItemSelect(v.getTag().toString());
        }
        public interface  IMyViewHolderClicks{
            public void onItemSelect(String idp);
        }
    }


}

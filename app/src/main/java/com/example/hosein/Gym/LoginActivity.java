package com.example.hosein.Gym;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.pnikosis.materialishprogress.ProgressWheel;

import Tools.Message;
import Tools.Session;
import Tools.Utils;

public class LoginActivity extends AppCompatActivity {

    EditText usernameTxt,passwordTxt;
    TextView  signInLable,Help , signUp , skipLogin;
    RelativeLayout signInBtn;
    ProgressWheel signInLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        usernameTxt=(EditText)findViewById(R.id.usernameTxt);
        passwordTxt=(EditText)findViewById(R.id.passwordTxt);
        Help =(TextView)findViewById(R.id.HelpLabel);
        signInBtn =(RelativeLayout)findViewById(R.id.signInBtn);
        signInLable=(TextView)findViewById(R.id.signInLable);
        skipLogin=(TextView)findViewById(R.id.skipLoginBtn);
        signUp =(TextView)findViewById(R.id.goToSignUpLabel);
        signInLoading=(ProgressWheel)findViewById(R.id.signInLoading);
        signInLoading.setBarColor(Color.rgb(240,240,240));
        signInLoading.setBarWidth(3);
        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login(usernameTxt.getText().toString(), passwordTxt.getText().toString(), "");
            }
        });
        skipLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GotoHome();
            }
        });
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoRegister = new Intent(LoginActivity.this,RegisterActivity.class);
                LoginActivity.this.startActivity(gotoRegister);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

    }

    private  void GotoHome()
    {

        Intent gotoHome = new Intent(LoginActivity.this,HomeActivity.class);
        LoginActivity.this.startActivity(gotoHome);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_right);
        finish();
    }

    private void login(String userName , String passWord , String captcha )
    {
        signInLable.setText("");
        signInLoading.spin();
        Ion.with(getBaseContext())
                .load(Utils.Domain + "/api/user/login")
                .setTimeout(30000)
                .setBodyParameter("username",userName)
                .setBodyParameter("password",passWord)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {

                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            Gson gson = new Gson();
                            Message msg = gson.fromJson(result, Message.class);
                            if (msg.type == 0) {
                                Session session = new Session(getBaseContext());
                                session.SetSession("userId", msg.data);
                                GotoHome();
                                //loadUserProfile();
                            } else {
                                YoYo.with(Techniques.Shake).playOn(usernameTxt);
                                YoYo.with(Techniques.Shake).playOn(passwordTxt);
                                Toast.makeText(getBaseContext(), "Invalid username or password", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception exp) {
                            Toast.makeText(getBaseContext(), "Sign In Error", Toast.LENGTH_SHORT).show();
                        } finally {
                            signInLoading.stopSpinning();
                            signInLable.setText("Sign In");
                        }
                    }
                });

    }


    public  void loadUserProfile()
    {
        Session _session = new Session(getBaseContext());
        String userId= _session.getSession("userId");
        Ion.with( getBaseContext())
                .load(Utils.Domain + "/MobileApp/user/getProfile?userId=" + userId)
                .setTimeout(30000)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            Log.d("cando", " infooooooo  " + result.toString());
                            Session _session = new Session(getBaseContext().getApplicationContext());
                            _session.SetSession("jsonedUserProfile", result.toString());

                        } catch (Exception exp) {
                            Log.d("cando", "exception in user profile data receive");
                        }

                    }
                });
    }



}


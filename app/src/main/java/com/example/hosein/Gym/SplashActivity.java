package com.example.hosein.Gym;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import Tools.Utils;

public class SplashActivity extends AppCompatActivity {

    boolean isConnected=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ImageView splash = (ImageView)findViewById(R.id.splashIMG);

       Utils utils = new Utils(getApplicationContext());
        if(utils.isOnline())
        {
           // Toast.makeText(getApplicationContext(),"is online",Toast.LENGTH_SHORT).show();
            isConnected=true;
        }


        if(isConnected) {
            YoYo.with(Techniques.FadeIn).duration(300).playOn(splash);
            YoYo.with(Techniques.FadeOut).delay(2000).duration(300).withListener(new com.nineoldandroids.animation.Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(com.nineoldandroids.animation.Animator animation) {

                }

                @Override
                public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                    Intent gotoLogin = new Intent(SplashActivity.this, LoginActivity.class);
                    SplashActivity.this.overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                    SplashActivity.this.startActivity(gotoLogin);
                    finish();
                }

                @Override
                public void onAnimationCancel(com.nineoldandroids.animation.Animator animation) {

                }

                @Override
                public void onAnimationRepeat(com.nineoldandroids.animation.Animator animation) {

                }
            }).playOn(splash);
        }

    }

}

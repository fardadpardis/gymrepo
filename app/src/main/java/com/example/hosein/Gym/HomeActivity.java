package com.example.hosein.Gym;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

public class HomeActivity extends AppCompatActivity {

    ImageView videoBtn , newsBtn , articleBtn , profileBtn , logoutBtn , faveBtn , shopBtn , calendarBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        videoBtn =(ImageView)findViewById(R.id.videoBtn);
        newsBtn =(ImageView)findViewById(R.id.newsBtn);
        articleBtn =(ImageView)findViewById(R.id.articleBtn);
        profileBtn =(ImageView)findViewById(R.id.profileBtn);
        logoutBtn =(ImageView)findViewById(R.id.logoutBtn);
        faveBtn =(ImageView)findViewById(R.id.faveBtn);
        shopBtn =(ImageView)findViewById(R.id.shopBtn);
        calendarBtn =(ImageView)findViewById(R.id.calendarBtn);
        videoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YoYo.with(Techniques.Flash).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        Intent gotoMain = new Intent(HomeActivity.this,MainActivity.class);
                        gotoMain.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        HomeActivity.this.startActivity(gotoMain);
                        finish();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).playOn(v);
            }
        });

        newsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YoYo.with(Techniques.Flash).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        Intent gotoMain = new Intent(HomeActivity.this,MainActivity.class);
                        gotoMain.putExtra("loadModule","news");
                        gotoMain.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        HomeActivity.this.startActivity(gotoMain);
                        finish();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).playOn(v);
            }
        });

        articleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                YoYo.with(Techniques.Flash).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        Intent gotoMain = new Intent(HomeActivity.this,MainActivity.class);
                        gotoMain.putExtra("loadModule","articles");
                        gotoMain.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        HomeActivity.this.startActivity(gotoMain);
                        finish();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).playOn(v);
            }
        });

    }

}

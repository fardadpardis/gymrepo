package com.example.hosein.Gym;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class ScannerFragment extends Fragment implements ZBarScannerView.ResultHandler {

    private ZBarScannerView mScannerView;
    private OnFragmentInteractionListener mListener;
    FrameLayout qrWrapper;
    public ScannerFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ScannerFragment newInstance() {
        ScannerFragment fragment = new ScannerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_scanner, container, false);
        qrWrapper = (FrameLayout)v.findViewById(R.id.qrWrapper);

        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        Double width =(displayMetrics.widthPixels *0.8);
        Double height =(displayMetrics.heightPixels *0.3);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width.intValue(),height.intValue());
        qrWrapper.setLayoutParams(lp);

        mScannerView=(ZBarScannerView)v.findViewById(R.id.qrScanner);
        return  v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void handleResult(Result rawResult) {
        try {
            // Do something with the result here
            Log.v("qr", rawResult.getContents()); // Prints scan results
            Log.v("Qr", rawResult.getBarcodeFormat().getName()); // Prints the scan format (qrcode, pdf417 etc.)
            mListener.returnCode(rawResult.getContents());


        }
        catch (Exception exp)
        {
            Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_LONG).show();
            // If you would like to resume scanning, call this method below:
            mScannerView.resumeCameraPreview(this);
        }
    }


    public interface OnFragmentInteractionListener {
        void returnCode(String code);
    }



}

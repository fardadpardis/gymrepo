package com.example.hosein.Gym;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.pnikosis.materialishprogress.ProgressWheel;

import Tools.Message;
import Tools.Session;
import Tools.Utils;

public class RegisterActivity extends AppCompatActivity {


    EditText usernameTxt,passwordTxt , phoneTxt , emailTxt;
    TextView registerLable,signInLabel;
    ImageView backBtn;
    RelativeLayout registerBtn;
    ProgressWheel registerLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        usernameTxt = (EditText) findViewById(R.id.usernameTxt);
        passwordTxt = (EditText) findViewById(R.id.passwordTxt);
        phoneTxt = (EditText) findViewById(R.id.phoneTxt);
        emailTxt = (EditText) findViewById(R.id.emailTxt);
        registerLable = (TextView) findViewById(R.id.registerLabel);
        signInLabel = (TextView) findViewById(R.id.signInLabel);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        registerBtn = (RelativeLayout) findViewById(R.id.registerBtn);
        registerLoading = (ProgressWheel) findViewById(R.id.registerLoading);
        registerLoading.setBarColor(Color.rgb(240, 240, 240));
        registerLoading.setBarWidth(3);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoLogin();
            }
        });
        signInLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoLogin();
            }
        });


        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Register();
            }
        });
    }
    private void  gotoLogin() {
        Intent gotoLogin = new Intent(RegisterActivity.this, LoginActivity.class);
        RegisterActivity.this.startActivity(gotoLogin);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private void Register() {
        registerLable.setText("");
        registerLoading.spin();
        Ion.with(getBaseContext())
                .load(Utils.Domain + "/api/user/Register")
                .setTimeout(30000)
                .setBodyParameter("username", usernameTxt.getText().toString())
                .setBodyParameter("password", passwordTxt.getText().toString())
                .setBodyParameter("phone", phoneTxt.getText().toString())
                .setBodyParameter("email", emailTxt.getText().toString())
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            Gson gson = new Gson();

                            Toast.makeText(getBaseContext(), result.toString(), Toast.LENGTH_LONG).show();
                            Message msg = gson.fromJson(result, Message.class);
                            if (msg.type == 0) {
                                Session session = new Session(getBaseContext());
                                session.SetSession("userId", msg.data);
                                gotoLogin();
                            } else {
                                YoYo.with(Techniques.Shake).playOn(usernameTxt);
                                YoYo.with(Techniques.Shake).playOn(passwordTxt);
                                YoYo.with(Techniques.Shake).playOn(phoneTxt);
                                YoYo.with(Techniques.Shake).playOn(emailTxt);
                                Toast.makeText(getBaseContext(), msg.text, Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception exp) {
                            Toast.makeText(getBaseContext(), "Sign Up Error", Toast.LENGTH_SHORT).show();
                        } finally {
                            registerLoading.stopSpinning();
                            registerLable.setText("Continue");
                        }
                    }
                });
    }
}

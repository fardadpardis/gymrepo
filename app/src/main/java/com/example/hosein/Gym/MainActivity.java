package com.example.hosein.Gym;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.StreamBody;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import java.io.IOException;
import java.util.List;

import Adapter.MediaRecycleViewAdapter;
import CustomVideoView.ResizeSurfaceView;
import CustomVideoView.VideoControllerView;
import Model.Media;
import Modules.ArticleGroups;
import Modules.ArticlesList;
import Modules.EquipmentDetail;
import Modules.MediaDetail;
import Modules.MediaGroups;
import Modules.MediaList;
import Modules.MostPopularMedias;
import Modules.NewsDetail;
import Modules.NewsList;
import NavigationDrawer.NavigationDrawerFragment;
import Scanner.ScannerDialog;
import Tools.Session;
import Tools.Utils;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , NavigationDrawerFragment.OnFragmentInteractionListener
        ,ScannerDialog.OnFragmentInteractionListener , MostPopularMedias.OnFragmentInteractionListener , MediaGroups.OnFragmentInteractionListener
, MediaDetail.OnFragmentInteractionListener , EquipmentDetail.OnFragmentInteractionListener , MediaList.OnFragmentInteractionListener , ArticleGroups.OnFragmentInteractionListener
,ArticlesList.OnFragmentInteractionListener , NewsList.OnFragmentInteractionListener {



  //  private OrientationListener orientationListener;


    //public variables
    DrawerLayout drawer;
    ImageView hamburgerBtn , videoPreView;
    FloatingActionButton fab ;
    LinearLayout mostPopularWrapper;
    LinearLayout mediaGroupWrapper;
    ScrollView mainScrollView;

    NavigationDrawerFragment navigationDrawerFragment;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


       // orientationListener = new OrientationListener(this);

        //Toast.makeText(getApplicationContext(),Utils.Domain,Toast.LENGTH_LONG).show();

        mainScrollView=(ScrollView)findViewById(R.id.mainScrollView);

        mostPopularWrapper= (LinearLayout)findViewById(R.id.mostPopularWrapper);
        mediaGroupWrapper=(LinearLayout)findViewById(R.id.mediaGroupWrapper);
        videoPreView=(ImageView)findViewById(R.id.videoPreView);
        videoPreView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);



        videoPreView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent playVideo = new Intent(MainActivity.this,VideoPlayerActivity.class);
                playVideo.putExtra("uri",v.getTag().toString());
                MainActivity.this.startActivity(playVideo);
            }
        });



        fab= (FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ScannerDialog qr = ScannerDialog.newInstance();
                qr.show(getFragmentManager(), "qr");
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        hamburgerBtn = (ImageView) toolbar.findViewById(R.id.hamburgerBtn);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false); //disable "hamburger to arrow" drawable
        toggle.syncState();
        navigationDrawerFragment = NavigationDrawerFragment.newInstance(MainActivity.this);
        FragmentManager fm = getSupportFragmentManager();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        fm.beginTransaction().add(R.id.nav_view, navigationDrawerFragment,"navigationFragment").commit();
        hamburgerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(navigationView)) {
                    drawer.closeDrawer(navigationView);
                } else {
                    drawer.openDrawer(navigationView);
                }
            }
        });
        if(getIntent().getExtras()!=null)
        {
            if(getIntent().getExtras().get("loadModule").equals("news"))
            {
                gotoNews();
            }
            else if (getIntent().getExtras().get("loadModule").equals("articles"))
            {
                gotoArticleGroups();
            }
        }
        getMostPopular();
        loadMediaGroup();
    }


    @Override protected void onStart() {
        super.onStart();
    }

    @Override protected void onStop() {
        super.onStop();

    }

    @Override protected void onPause() {
        super.onPause();
       // mMediaPlayer.pause();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void onResume()
    {
        super.onResume();

       // mMediaPlayer.prepareAsync();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState); 
    }


    public  void  loadMostPopular(List<Media> mostPopularList)
    {
        FragmentManager fm = getSupportFragmentManager();
        MostPopularMedias _mostPopular = MostPopularMedias.newInstance(mostPopularList);
        fm.beginTransaction().add(R.id.mostPopularWrapper,_mostPopular,"mostPopular").commit();
    }

    public  void  loadMediaGroup()
    {
        FragmentManager fm = getSupportFragmentManager();
        MediaGroups _medaiGroups = MediaGroups.newInstance("",false);
        fm.beginTransaction().add(R.id.mediaGroupWrapper,_medaiGroups,"mediaGroup").commit();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void reloadNavDrawer() {

        drawer.closeDrawer(Gravity.RIGHT);
        navigationDrawerFragment = NavigationDrawerFragment.newInstance(MainActivity.this);
        FragmentManager fm = getSupportFragmentManager();
        if(fm.findFragmentByTag("navigationFragment")!=null) {
            Fragment oldNavigationView = fm.findFragmentByTag("navigationFragment");
            fm.beginTransaction().remove(oldNavigationView).commit();
            fm.beginTransaction().add(R.id.nav_view, navigationDrawerFragment, "navigationFragment").commit();
        }
    }

    @Override
    public void closeNavDrawer() {
        drawer.closeDrawer(Gravity.RIGHT);
    }

    @Override
    public void gotoArticleGroups() {
        closeNavDrawer();
        ArticleGroups articleGroups = ArticleGroups.newInstance();
        FragmentManager fm = getSupportFragmentManager();
        if(fm.findFragmentByTag("ArticleGroupFragment")!=null) {
            Fragment oldArticleGroup = fm.findFragmentByTag("ArticleGroupFragment");
            fm.beginTransaction().remove(oldArticleGroup).commit();
        }
            fm.beginTransaction().add(R.id.MediaDetailContainer, articleGroups, "ArticleGroupFragment").addToBackStack("ArticleGroupFragment").commit();

    }

    @Override
    public void gotoNews() {
        closeNavDrawer();
        FragmentManager fm = getSupportFragmentManager();
        if(fm.findFragmentByTag("newsList")==null) {
            NewsList fragment = NewsList.newInstance();
            fm.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).add(R.id.MediaDetailContainer, fragment, "newsList").addToBackStack("newsList").commit();
        }
        else
        {
            Fragment old = fm.findFragmentByTag("newsList");
            fm.beginTransaction().remove(old).commit();
            NewsList fragment = NewsList.newInstance();
            fm.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).add(R.id.MediaDetailContainer, fragment, "newsList").addToBackStack("newsList").commit();

        }

    }

    @Override
    public void gotoSpecial(String filterBy, String title) {
        drawer.closeDrawer(Gravity.RIGHT);
    }




    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // controller.show();
        return false;
    }

//    @Override
//    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
//        mVideoHeight = mp.getVideoHeight();
//        mVideoWidth = mp.getVideoWidth();
//        if (mVideoHeight > 0 && mVideoWidth > 0)
//          mVideoSurface.adjustSize(mContentView.getWidth(), mContentView.getHeight(), mMediaPlayer.getVideoWidth(), mMediaPlayer.getVideoHeight());
//
//    }
//
//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        if (mVideoWidth > 0 && mVideoHeight > 0)
//           mVideoSurface.adjustSize(getDeviceWidth(this), getDeviceHeight(this), mVideoSurface.getWidth(), mVideoSurface.getHeight());
//
//        if(newConfig.orientation==Configuration.ORIENTATION_LANDSCAPE)
//        {
//            FrameLayout.LayoutParams videoWrapperLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,getDeviceHeight(MainActivity.this)-(int) getResources().getDimension(R.dimen.tool_bar_height));
//            mContentView.setLayoutParams(videoWrapperLP);
//            scrollToTop();
//        }
//        else if(newConfig.orientation==Configuration.ORIENTATION_PORTRAIT)
//        {
//            FrameLayout.LayoutParams videoWrapperLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,(int) getResources().getDimension(R.dimen.surface_view_height));
//
//            mContentView.setLayoutParams(videoWrapperLP);
//            scrollToTop();
//        }
//    }



    public static int getDeviceWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(mDisplayMetrics);
        return mDisplayMetrics.widthPixels;
    }

    public static int getDeviceHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(mDisplayMetrics);
        return mDisplayMetrics.heightPixels;
    }




    /**
     * Implement VideoMediaController.MediaPlayerControl
     */




//
//    @Override
//    public void toggleFullScreen() {
//        if(isFullScreen()){
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//            FrameLayout.LayoutParams videoWrapperLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,(int) getResources().getDimension(R.dimen.surface_view_height)  );
//            Log.d("gym he:",R.dimen.surface_view_height+"");
//            mContentView.setLayoutParams(videoWrapperLP);
//        }else {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//            FrameLayout.LayoutParams videoWrapperLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,getDeviceHeight(MainActivity.this)-(int) getResources().getDimension(R.dimen.tool_bar_height));
//            mContentView.setLayoutParams(videoWrapperLP);
//        }
//    }




    @Override
    public void returnCode(String code) {
        //Toast.makeText(MainActivity.this,code,Toast.LENGTH_LONG).show();
        FragmentManager fm = getSupportFragmentManager();
        if(fm.findFragmentByTag("equipmentDetail")==null) {
            EquipmentDetail fragment = EquipmentDetail.newInstance(code);
            fm.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).add(R.id.MediaDetailContainer, fragment, "equipmentDetail").addToBackStack("equipmentDetail").commit();
        }
        else
        {
            Fragment old = fm.findFragmentByTag("equipmentDetail");
            fm.beginTransaction().remove(old).commit();
            EquipmentDetail fragment = EquipmentDetail.newInstance(code);
            fm.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).add(R.id.MediaDetailContainer, fragment, "equipmentDetail").addToBackStack("equipmentDetail").commit();

        }
    }

    @Override
    public void closeProductDetail() {

    }

    @Override
    public void stopVideoPlayer() {

    }

    @Override
    public void hideSurfaceView() {

    }

    @Override
    public void showSurfaceView() {

    }


    public void getMostPopular()
    {
        Session session = new Session(MainActivity.this);
        Ion.with(MainActivity.this).load(Utils.Domain+"/api/media/mostPopular?userId="+session.getSession("userId"))
                .setTimeout(30000)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        try {
                            Gson gson = new Gson();
                            TypeToken<List<Media>> type = new TypeToken<List<Media>>() {
                            };
                            List<Media> mostPopularList = gson.fromJson(result, type.getType());
                            if(mostPopularList!=null)
                            {
                             try {

                                 loadMostPopular(mostPopularList);
                                 videoPreView.setTag(Utils.Domain+ mostPopularList.get((0)).uri);
                                 UrlImageViewHelper.setUrlDrawable(videoPreView, Utils.GetImageUrl(Utils.Domain + mostPopularList.get(0).thumbUrl), R.drawable.logo, new UrlImageViewCallback() {
                                     @Override
                                     public void onLoaded(ImageView imageView, Bitmap bitmap, String s, boolean b) {
                                         imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                     }
                                 });
                             }
                             catch (Exception exp)
                             {

                             }

                            }

                        }catch (Exception exp)
                        {

                        }
                    }
                });
    }


    public  void  scrollToTop()
    {
        mainScrollView.scrollTo(0, 0);
    }



    @Override
    public void showArticlesList(String groupId) {

        FragmentManager fm = getSupportFragmentManager();
        if(fm.findFragmentByTag("articlesList")==null) {
            ArticlesList fragment = ArticlesList.newInstance(groupId);
            fm.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).add(R.id.MediaDetailContainer, fragment, "articlesList").addToBackStack("articlesList").commit();
        }
        else
        {
            Fragment old = fm.findFragmentByTag("articlesList");
            fm.beginTransaction().remove(old).commit();
            ArticlesList fragment = ArticlesList.newInstance(groupId);
            fm.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).add(R.id.MediaDetailContainer, fragment, "articlesList").addToBackStack("articlesList").commit();

        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void loadNewsDetail(String newsObject) {
        FragmentManager fm = getSupportFragmentManager();
        if(fm.findFragmentByTag("newsDetail")==null) {
            NewsDetail fragment = NewsDetail.newInstance(newsObject);
            fm.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).add(R.id.MediaDetailContainer, fragment, "newsDetail").addToBackStack("newsDetail").commit();
        }
        else
        {
            Fragment old = fm.findFragmentByTag("newsDetail");
            fm.beginTransaction().remove(old).commit();
            ArticlesList fragment = ArticlesList.newInstance(newsObject);
            fm.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).add(R.id.MediaDetailContainer, fragment, "newsDetail").addToBackStack("newsDetail").commit();
        }
    }

    public void getMostPopulars()
    {
        Session session = new Session(MainActivity.this);
        Ion.with(MainActivity.this).load(Utils.Domain+"/api/media/mostPopular?userId="+session.getSession("userId"))
                .setTimeout(30000)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        try {
                            Gson gson = new Gson();
                            TypeToken<List<Media>> type = new TypeToken<List<Media>>() {
                            };
                            List<Media> mostPopularList = gson.fromJson(result, type.getType());
                            if(mostPopularList!=null)
                            {
                                 loadMostPopular(mostPopularList);
                            }
                        }catch (Exception exp)
                        {

                        }
                    }
                });
    }

//
//    private class OrientationListener extends OrientationEventListener {
//        final int ROTATION_O    = 1;
//        final int ROTATION_90   = 2;
//        final int ROTATION_180  = 3;
//        final int ROTATION_270  = 4;
//
//        private int rotation = 0;
//        public OrientationListener(Context context) { super(context); }
//
//        @Override public void onOrientationChanged(int orientation) {
//            if( (orientation < 35 || orientation > 325) && rotation!= ROTATION_O){ // PORTRAIT
//                rotation = ROTATION_O;
//                FrameLayout.LayoutParams videoWrapperLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,(int) getResources().getDimension(R.dimen.surface_view_height));
//
//                mContentView.setLayoutParams(videoWrapperLP);
//                scrollToTop();
//            }
//            else if( orientation > 145 && orientation < 215 && rotation!=ROTATION_180){ // REVERSE PORTRAIT
//                rotation = ROTATION_180;
//                FrameLayout.LayoutParams videoWrapperLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,(int) getResources().getDimension(R.dimen.surface_view_height));
//
//                mContentView.setLayoutParams(videoWrapperLP);
//                scrollToTop();
//            }
//            else if(orientation > 55 && orientation < 125 && rotation!=ROTATION_270){ // REVERSE LANDSCAPE
//                rotation = ROTATION_270;
//
//                FrameLayout.LayoutParams videoWrapperLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,getDeviceHeight(MainActivity.this)-(int) getResources().getDimension(R.dimen.tool_bar_height));
//                mContentView.setLayoutParams(videoWrapperLP);
//                scrollToTop();
//
//            }
//            else if(orientation > 235 && orientation < 305 && rotation!=ROTATION_90){ //LANDSCAPE
//                rotation = ROTATION_90;
//
//                FrameLayout.LayoutParams videoWrapperLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,getDeviceHeight(MainActivity.this)-(int) getResources().getDimension(R.dimen.tool_bar_height));
//                mContentView.setLayoutParams(videoWrapperLP);
//                scrollToTop();
//
//            }
//        }
//    }
}







package NavigationDrawer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hosein.Gym.LoginActivity;
import com.example.hosein.Gym.MainActivity;
import com.example.hosein.Gym.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

import Adapter.NavigationDrawerAdapter;
import Tools.Session;
import Tools.Utils;
import User.GymUser;
import de.hdodenhof.circleimageview.CircleImageView;


public class NavigationDrawerFragment extends android.support.v4.app.Fragment {

    Context context ;
    GymUser user; // user object
    ListView sideMenuListView; // listview for demonstrate menu items
    TextView   username , email;
    CircleImageView UserAvatar; // user avatar
    private OnFragmentInteractionListener mListener; // callback interFace


    // static instantiate of NavigationDrawerFragment
    public static NavigationDrawerFragment newInstance (Context context)
    {
        NavigationDrawerFragment newND  = new NavigationDrawerFragment();
       // newND.context=context;
        return newND;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        Session _session = new Session(getActivity());
        final View v = inflater.inflate(R.layout.navigation_drawer_fragment, container, false);
        sideMenuListView = (ListView)v.findViewById(R.id.navItemListView);
        username= (TextView)v.findViewById(R.id.userNameTxt);
        email= (TextView)v.findViewById(R.id.userEmailTxt);
        UserAvatar = (CircleImageView)v.findViewById(R.id.profile_image);
        loadUserProfile();
        List<NavigationDrawerItem> sideMenuItems = new ArrayList<>();
        sideMenuItems.add(new NavigationDrawerItem("صفحه اصلی","home",R.drawable.home));
        sideMenuItems.add(new NavigationDrawerItem("محبوبترین ها","fav",R.drawable.fav));
        sideMenuItems.add(new NavigationDrawerItem("اخبار","news",R.drawable.nav_news));
        sideMenuItems.add(new NavigationDrawerItem("مقالات","articles",R.drawable.article_menu));
        sideMenuItems.add(new NavigationDrawerItem("علاقه مندی ها","wish",R.drawable.wish));
        if( _session.getSession("userId")!=null)
        {
            sideMenuItems.add(new NavigationDrawerItem("خروج","exit",R.drawable.exit));
        }
        else
        {
            sideMenuItems.add(new NavigationDrawerItem("ورود","login",R.drawable.exit));
        }
        NavigationDrawerAdapter adapter = new NavigationDrawerAdapter(getContext(),sideMenuItems);
        sideMenuListView.setAdapter(adapter);
        sideMenuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (view.getTag().toString())
                {
                    case "exit" : {
                        Session _session = new Session(getActivity());
                        _session.SetSession("userId",null);
                        _session.SetSession("fullName",null);
                        _session.SetSession("jsonedUserProfile",null);
                        mListener.reloadNavDrawer();
                        break;
                    }

                    case "login" : {
                        Intent gotoLogin = new Intent(getActivity(), LoginActivity.class);
                        getActivity().startActivity(gotoLogin);
                        mListener.closeNavDrawer();
                        mListener.reloadNavDrawer();
                        break;
                    }

                    case "home" : {
                        FragmentManager FM = getActivity().getSupportFragmentManager();
                        for(int i = 0; i < FM.getBackStackEntryCount(); ++i) {
                            FM.popBackStack();
                            mListener.closeNavDrawer();
                        }
                        mListener.closeNavDrawer();
                        break;
                    }

                    case "fav" : {
                         mListener.gotoSpecial("","محبوبترین ها");
                        break;
                    }
                    case "news" : {
                        mListener.gotoNews();
                        break;
                    }
                    case "articles" : {

                        mListener.gotoArticleGroups();
                        break;
                    }
                }
            }
        });

        return  v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        if (context instanceof MainActivity){
            activity=(Activity) context;
            try {
                mListener = (OnFragmentInteractionListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement OnFragmentInteractionListener");
            }
        }
        else
        {
            throw new ClassCastException("the activity must implement OnFragmentInteractionListener");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
          void  reloadNavDrawer();
          void  closeNavDrawer();
          void gotoArticleGroups();
          void gotoNews();
          void gotoSpecial(String filterBy, String title);
    }

    // load user profile
    public  void loadUserProfile()
    {
        Session _session = new Session(getActivity());
        String userId= _session.getSession("userId");
        Ion.with(getActivity())
                .load(Utils.Domain + "/MobileApp/user/getProfile?userId=" + userId)
                .setTimeout(30000)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                    try
                    {
                        Gson gson = new Gson();
//                        CustomUserProfile userProfile = gson.fromJson(result, CustomUserProfile.class);
//                        username.setText(userProfile.fullName);
//                        email.setText(userProfile.email);
//                        UrlImageViewHelper.setUrlDrawable(UserAvatar, userProfile.userPicUrl, R.drawable.avatar);
                    }
                    catch (Exception exp)
                    {
                        Log.d("cando","exception in user profile data receive");
                    }

                    }
                });
         }

}

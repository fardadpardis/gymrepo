package Scanner;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.hosein.Gym.R;
import com.example.hosein.Gym.ScannerFragment;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

/**
 * Created by hosein on 3/7/2016.
 */
public class ScannerDialog extends DialogFragment implements ZBarScannerView.ResultHandler  {
    private ZBarScannerView mScannerView;
    private OnFragmentInteractionListener mListener;
    FrameLayout qrWrapper;
    public ScannerDialog() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ScannerDialog newInstance() {
        ScannerDialog fragment = new ScannerDialog();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_scanner, container, false);
        qrWrapper = (FrameLayout)v.findViewById(R.id.qrWrapper);

        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        Double width =(displayMetrics.widthPixels *0.8);
        Double height =(displayMetrics.heightPixels *0.3);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width.intValue(),height.intValue());
        qrWrapper.setLayoutParams(lp);

        mScannerView=(ZBarScannerView)v.findViewById(R.id.qrScanner);
        return  v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void handleResult(Result rawResult) {
     try {

        OnFragmentInteractionListener activity = (OnFragmentInteractionListener) getActivity();
        activity.returnCode(rawResult.getContents());
        dismiss();
        }
        catch (Exception exp)
        {
            Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_LONG).show();
            // If you would like to resume scanning, call this method below:
            mScannerView.resumeCameraPreview(this);
        }
    }


    public interface OnFragmentInteractionListener {
        void returnCode(String code);
    }


}

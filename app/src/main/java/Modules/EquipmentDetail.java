package Modules;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.hosein.Gym.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import Adapter.EquipmentDetailMediaRecycleViewAdapter;
import Adapter.MediaRecycleViewAdapter;
import Model.Equipment;
import Model.Media;
import Tools.SansTextView;
import Tools.Session;
import Tools.Utils;
import Tools.YekanTextView;


public class EquipmentDetail extends Fragment {

    private OnFragmentInteractionListener mListener;
    SansTextView equipmentName;
    YekanTextView equipmentDescription;
    ImageView equipmentImage;
    String equipmentCode;
    StaggeredGridLayoutManager SGLM;
    RecyclerView mediasRecyclerView;
    ProgressWheel equipmentDetailLoading;
    public EquipmentDetail() {
        // Required empty public constructor
    }


    public static EquipmentDetail newInstance(String equipmentCode) {
        EquipmentDetail fragment = new EquipmentDetail();
        Bundle args = new Bundle();
        args.putString("equipmentCode", equipmentCode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            equipmentCode = getArguments().getString("equipmentCode");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_equipment_detail, container, false);
        SGLM = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        mediasRecyclerView=(RecyclerView)v.findViewById(R.id.equipmentMedias);
        equipmentDescription=(YekanTextView)v.findViewById(R.id.equipmentDescription);
        equipmentName=(SansTextView)v.findViewById(R.id.equipmentName);
        equipmentImage=(ImageView)v.findViewById(R.id.equipmentImage);
        mediasRecyclerView.setLayoutManager(SGLM);
        equipmentDetailLoading=(ProgressWheel)v.findViewById(R.id.equipmentDetailLoading);
        equipmentDetailLoading.setBarColor(Color.argb(255,245,245,245));
        equipmentDetailLoading.setBarWidth(6);
        equipmentDetailLoading.spin();
        loadEquipmentData();



        return  v;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {

    }

    public  void loadEquipmentData()
    {
        Ion.with(getContext())
                .load(Utils.Domain+"/api/Equipments/getEquipment?code="+equipmentCode)
                .setTimeout(30000)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            Gson gson = new Gson();
                            Equipment _equipment = gson.fromJson(result, Equipment.class);
                           // Toast.makeText(getContext(),result.toString(),Toast.LENGTH_LONG).show();
                            Log.d("gym",result.toString());
                            equipmentName.setText(_equipment.name);
                            equipmentDescription.setText(_equipment.description);
                            UrlImageViewHelper.setUrlDrawable(equipmentImage,Utils.GetImageUrl(Utils.Domain + _equipment.iconUrl), R.drawable.logo, new UrlImageViewCallback() {
                                @Override
                                public void onLoaded(ImageView imageView, Bitmap bitmap, String s, boolean b) {
                                    imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                                    YoYo.with(Techniques.FadeIn).playOn(imageView);
                                }
                            });
                            loadEquipmentMedias(_equipment.id);
                        }
                        catch (Exception exp)
                        {

                        }
                    }
                });



    }

    private void loadEquipmentMedias(int equipmentId)
    {
        Session _session =new Session(getContext());
        Ion.with(getContext())
                .load(Utils.Domain+"/api/media/List?equipmentId="+equipmentId+"&userId="+_session.getSession("userId"))
                .setTimeout(30000)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        try {

                           // Toast.makeText(getContext(),result.toString(),Toast.LENGTH_LONG).show();
                            Log.d("gym media " , result.toString());
                            Gson gson = new Gson();
                            List<Media> mediaList;
                            TypeToken<List<Media>> type =new TypeToken<List<Media>>(){};
                            mediaList = gson.fromJson(result, type.getType());
                            if(mediaList!=null && mediaList.size()>0) {
                                EquipmentDetailMediaRecycleViewAdapter _adapter = new EquipmentDetailMediaRecycleViewAdapter(getContext(), mediaList);
                                mediasRecyclerView.setAdapter(_adapter);

                            }
                            equipmentDetailLoading.stopSpinning();
                        }
                        catch (Exception exp)
                        {

                        }
                    }
                });
    }
}

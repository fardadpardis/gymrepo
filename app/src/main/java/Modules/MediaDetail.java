package Modules;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.hosein.Gym.R;
import com.example.hosein.Gym.VideoPlayerActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import CustomVideoView.ResizeSurfaceView;
import CustomVideoView.VideoControllerView;
import Model.Media;
import NavigationDrawer.NavigationDrawerFragment;
import Scanner.ScannerDialog;
import Tools.Session;
import Tools.Utils;

public class MediaDetail extends Fragment
  {
      Media media;
      String mediaUri;
      FrameLayout videoSurfaceContainer;
      private OnFragmentInteractionListener mListener;
      TextView mediaTitle , mediaDuration ,mediaCalory , mediaWorkout , mediaLevel , mediaDescription;
      ImageView mediaIcon ,videoPreView;

    public MediaDetail() {
        // Required empty public constructor
    }
    public static MediaDetail newInstance(String media) {
        MediaDetail fragment = new MediaDetail();
        Bundle args = new Bundle();
        args.putString("media", media);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Gson gson = new Gson();
            media =gson.fromJson(getArguments().getString("media"),Media.class);
            mListener.stopVideoPlayer();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View v = inflater.inflate(R.layout.fragment_media_detail, container, false);
        mediaTitle=(TextView)v.findViewById(R.id.mediaTitle);
        mediaDescription=(TextView)v.findViewById(R.id.mediaDescription);
        mediaDuration=(TextView)v.findViewById(R.id.mediaDuration);
        mediaIcon=(ImageView)v.findViewById(R.id.mediaIcon);
        mediaCalory=(TextView)v.findViewById(R.id.mediaCalory);
        mediaLevel=(TextView)v.findViewById(R.id.mediaLevel);
        mediaWorkout=(TextView)v.findViewById(R.id.mediaWorkOut);
        videoSurfaceContainer=(FrameLayout)v.findViewById(R.id.videoSurfaceContainer);
        videoPreView = (ImageView)v.findViewById(R.id.videoPreView);


        videoPreView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent playVideo = new Intent(getActivity(), VideoPlayerActivity.class);
                playVideo.putExtra("uri",v.getTag().toString());
                getActivity().startActivity(playVideo);
            }
        });

        UrlImageViewHelper.setUrlDrawable(videoPreView, Utils.Domain + media.thumbUrl, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String s, boolean b) {
                imageView.setTag(media.uri);
            }
        });


        mediaTitle.setText(media.name);
        mediaDescription.setText(media.description);
        String tag = media.tag;
        String[] info =tag.split("-");
        if(info.length>1) {
            mediaDuration.setText(info[0]);
            mediaCalory.setText(info[1]);
            mediaLevel.setText(info[2]);
        }
        UrlImageViewHelper.setUrlDrawable(mediaIcon, Utils.GetImageUrl(Utils.Domain + media.thumbUrl), R.color.popular_media_bg, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String s, boolean b) {
                YoYo.with(Techniques.FadeIn).duration(500).playOn(imageView);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
        });
        return  v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void closeProductDetail();
        void stopVideoPlayer();
        void  hideSurfaceView();
        void  showSurfaceView();
    }


      @Override
      public  void onDestroy()
      {
          super.onDestroy();
          mListener.showSurfaceView();
      }


  }

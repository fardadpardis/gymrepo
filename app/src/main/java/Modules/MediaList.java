package Modules;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hosein.Gym.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import Adapter.MediaListRecycleViewAdapter;
import Adapter.MediaRecycleViewAdapter;
import Model.Media;
import Tools.Session;
import Tools.Utils;


public class MediaList extends Fragment {


    String groupId , equipmentId;
    private OnFragmentInteractionListener mListener;
    RecyclerView mediaListRecyclerView;
    ProgressWheel mediaListLoading;

    StaggeredGridLayoutManager mediaListLM;

    public MediaList() {
        // Required empty public constructor
    }


    public static MediaList newInstance(String groupId, String equipmentId) {
        MediaList fragment = new MediaList();
        Bundle args = new Bundle();
        args.putString("groupId", groupId);
        args.putString("equipmentId", equipmentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            equipmentId = getArguments().getString("equipmentId");
            groupId = getArguments().getString("groupId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_media_list, container, false);
        mediaListRecyclerView=(RecyclerView)v.findViewById(R.id.mediaListRecyclerView);
        mediaListLM =new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        mediaListRecyclerView.setLayoutManager(mediaListLM);

        mediaListLoading=(ProgressWheel)v.findViewById(R.id.mediaListLoading);
        mediaListLoading.setBarColor(Color.argb(255, 240, 240, 240));
        mediaListLoading.setBarWidth(5);
        getMediaList();
        return  v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {

    }


    public void getMediaList()
    {
        mediaListLoading.spin();
        Session session = new Session(getContext());
        Ion.with(getContext()).load(Utils.Domain+"/api/media/list?userId="+session.getSession("userId")+"&groupId="+groupId+"&equipmentId="+equipmentId)
                .setTimeout(30000)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        try {
                            Log.d("gym",result.toString());
                            Gson gson = new Gson();
                            TypeToken<List<Media>> type = new TypeToken<List<Media>>() {
                            };
                            List<Media> mostPopularList = gson.fromJson(result, type.getType());
                            if(mostPopularList!=null)
                            {
                                MediaListRecycleViewAdapter _adapter = new MediaListRecycleViewAdapter(getContext(), mostPopularList);
                                mediaListRecyclerView.setAdapter(_adapter);
                            }
                            mediaListLoading.stopSpinning();
                        }catch (Exception exp)
                        {

                        }
                    }
                });
    }
}

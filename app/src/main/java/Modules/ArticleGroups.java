package Modules;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.hosein.Gym.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.Util;
import com.koushikdutta.async.future.ConvertFuture;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.List;

import Adapter.ArticleGroupAdapter;
import Model.Group;
import Tools.Utils;

public class ArticleGroups extends Fragment {

    ListView articleGroupList;
    private OnFragmentInteractionListener mListener;

    public ArticleGroups() {
        // Required empty public constructor
    }


    public static ArticleGroups newInstance( ) {
        ArticleGroups fragment = new ArticleGroups();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_article_groups, container, false);
        articleGroupList = (ListView)v.findViewById(R.id.articleGroupList);
        articleGroupList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mListener.showArticlesList(view.getTag()+"");
            }
        });
        loadArticleGroups();
        return  v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        void showArticlesList(String groupId);
    }

    void loadArticleGroups()
    {
        Ion.with(getContext())
                .load(Utils.Domain+"api/Article/GroupList")
                .setTimeout(30000)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        Gson gson = new Gson();
                        TypeToken<List<Group>> type = new TypeToken<List<Group>>() {
                        };
                        List<Group> groupList = gson.fromJson(result, type.getType());
                        if(groupList!=null && groupList.size()>0) {

                            ArticleGroupAdapter _adapter = new ArticleGroupAdapter(getContext(),groupList);
                            articleGroupList.setAdapter(_adapter);
                        }
                    }
                });
    }
}

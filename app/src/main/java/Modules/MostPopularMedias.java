package Modules;

import android.content.Context;
import android.graphics.Color;
import android.media.session.MediaSession;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.hosein.Gym.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import Adapter.MediaRecycleViewAdapter;
import Model.Media;
import Tools.Session;
import Tools.Utils;


public class MostPopularMedias extends Fragment {



    private OnFragmentInteractionListener mListener;
    RecyclerView MostPopularRecyclerView;
    ProgressWheel mostPopularLoding;
    List<Media> mostPopularList;
    LinearLayoutManager MostPopularLLM;


    public MostPopularMedias() {
        // Required empty public constructor
    }

    public static MostPopularMedias newInstance( List<Media> mostPopularList) {
        MostPopularMedias fragment = new MostPopularMedias();
        Gson gson = new Gson();
        String jsonedList = gson.toJson(mostPopularList);
        Bundle args = new Bundle();
        args.putString("jsonedList",jsonedList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null)
        {
            Gson gson = new Gson();
            TypeToken<List<Media>> token =new TypeToken<List<Media>>(){};
            mostPopularList = gson.fromJson(getArguments().getString("jsonedList"),token.getType());

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_most_popular_medias, container, false);
        MostPopularRecyclerView=(RecyclerView)v.findViewById(R.id.MostPopularRecyclerView);
        MostPopularLLM = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        MostPopularRecyclerView.setLayoutManager(MostPopularLLM);

        mostPopularLoding=(ProgressWheel)v.findViewById(R.id.mostPopularLoading);
        mostPopularLoding.setBarColor(Color.argb(255,240,240,240));
        mostPopularLoding.setBarWidth(5);
        loadMostPopulars(mostPopularList);
        return  v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

    }

    public void loadMostPopulars(List<Media> mostPopularList)
    {
        MediaRecycleViewAdapter _adapter = new MediaRecycleViewAdapter(getContext(), mostPopularList);
        MostPopularRecyclerView.setAdapter(_adapter);

    }
}

package Modules;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.hosein.Gym.R;
import com.google.gson.Gson;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import java.io.IOException;

import CustomVideoView.ResizeSurfaceView;
import CustomVideoView.VideoControllerView;
import Model.Document;
import Model.Media;
import Tools.SansTextView;
import Tools.Utils;
import Tools.YekanTextView;

public class NewsDetail extends Fragment
  {
      TextView newstitle , newsBrief ,newsText , author , date ;
      ImageView newsIcon ;
      Document newsObject;


    public NewsDetail() {
        // Required empty public constructor
    }
    public static NewsDetail newInstance(String newsObject) {
        NewsDetail fragment = new NewsDetail();
        Bundle args = new Bundle();
        args.putString("newsObject", newsObject);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Gson gson = new Gson();
            newsObject =gson.fromJson(getArguments().getString("newsObject"),Document.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View v = inflater.inflate(R.layout.news_detail, container, false);
        newstitle=(SansTextView)v.findViewById(R.id.newsTitle);
        newsBrief=(SansTextView)v.findViewById(R.id.newsBrief);
        newsText=(SansTextView)v.findViewById(R.id.newsText);
        newsIcon=(ImageView)v.findViewById(R.id.newsIcon);
        author=(YekanTextView)v.findViewById(R.id.newsAuthor);
        date=(YekanTextView)v.findViewById(R.id.newsDate);


        newstitle.setText(newsObject.title);
        newsText.setText(newsObject.text);
        if(newsObject.description.length()>90)
        {

            newsBrief.setText(newsObject.description.substring(0,90)+"...");
        }
        else {
            newsBrief.setText(newsObject.description);
        }
        author.setText("مدیریت باشگاه");
        date.setText(newsObject.tag);

        UrlImageViewHelper.setUrlDrawable(newsIcon, Utils.Domain + newsObject.thumbUri, new UrlImageViewCallback() {
            @Override
            public void onLoaded(ImageView imageView, Bitmap bitmap, String s, boolean b) {

            }
        });

        return  v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


      @Override
      public  void onDestroy()
      {
          super.onDestroy();
      }


  }

package Modules;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.hosein.Gym.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import Adapter.MediaGroupRecycleViewAdapter;
import Adapter.MediaRecycleViewAdapter;
import Model.Group;
import Model.Media;
import Tools.Utils;


public class MediaGroups extends Fragment {

    RecyclerView mediaGroupRecyclerView;
    ProgressWheel mediaGroupLoding;
    RelativeLayout media_group_wrapper;
    String parentId;
    boolean transparent;

    StaggeredGridLayoutManager mediaGroupLLM;

    private OnFragmentInteractionListener mListener;

    public MediaGroups() {
        // Required empty public constructor
    }

    public static MediaGroups newInstance(String parentId , boolean withBackground) {
        MediaGroups fragment = new MediaGroups();
        Bundle args = new Bundle();
        args.putString("parentId",parentId);
        args.putBoolean("transparent",withBackground);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null)
        {
            parentId=getArguments().getString("parentId");
            transparent=getArguments().getBoolean("transparent");
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_media_groups, container, false);
        mediaGroupRecyclerView=(RecyclerView)v.findViewById(R.id.mediaGroupRecyclerView);
        mediaGroupLoding=(ProgressWheel)v.findViewById(R.id.mediaGroupLoading);
        media_group_wrapper=(RelativeLayout)v.findViewById(R.id.media_group_wrapper);
        mediaGroupLoding.setBarColor(Color.argb(255,245,245,245));
        mediaGroupLoding.setBarWidth(3);
        mediaGroupLLM = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        mediaGroupRecyclerView.setLayoutManager(mediaGroupLLM);

        if(transparent)
        {
            //media_group_wrapper.setBackground();
        }

        getMediaGroups();
        return  v;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {

    }

    public void getMediaGroups()
    {
        mediaGroupLoding.spin();
        Ion.with(getContext()).load(Utils.Domain+"/api/media/GroupList?parentId="+parentId)
                .setTimeout(30000)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        try {
                            Gson gson = new Gson();
                            TypeToken<List<Group>> type = new TypeToken<List<Group>>() {
                            };
                            List<Group> groupList = gson.fromJson(result, type.getType());
                            if(groupList!=null && groupList.size()>0)
                            {
                                MediaGroupRecycleViewAdapter _adapter = new MediaGroupRecycleViewAdapter(getContext(), groupList);
                                mediaGroupRecyclerView.setAdapter(_adapter);
                                ViewGroup.LayoutParams params=mediaGroupRecyclerView.getLayoutParams();
                                params.height=(groupList.size())*((int)getResources().getDimension(R.dimen.media_group_card_height))/2;
                                if(groupList.size()%2 !=0)
                                {
                                    params.height+=(int)getResources().getDimension(R.dimen.media_group_card_height);
                                }
                                mediaGroupRecyclerView.setLayoutParams(params);
                                mediaGroupLoding.stopSpinning();
                            }
                            else
                            {
                                loadGroupVideos(parentId);
                            }
                        }catch (Exception exp)
                        {
                            mediaGroupLoding.stopSpinning();
                        }
                    }
                });
    }

    public void loadGroupVideos(final String GroupId)
    {
        Ion.with(getContext()).load(Utils.Domain+"/api/media/list?groupId="+GroupId)
                .setTimeout(30000)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        try {
                            Gson gson = new Gson();
                            TypeToken<List<Media>> type = new TypeToken<List<Media>>() {
                            };
                            List<Media> mediaList = gson.fromJson(result, type.getType());
                            if(mediaList!=null && mediaList.size()>0)
                            {
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                MediaList ML = MediaList.newInstance(GroupId,"");
                                fm.beginTransaction().setCustomAnimations(R.anim.enter_from_bottom,R.anim.exit_to_bottom).add(R.id.MediaDetailContainer,ML).addToBackStack("groupVideos").commit();
                            }
                            mediaGroupLoding.stopSpinning();
                        }catch (Exception exp)
                        {
                            mediaGroupLoding.stopSpinning();
                        }
                    }
                });
    }
}

package Modules;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.hosein.Gym.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.File;
import java.util.List;

import Adapter.ArticleListAdapter;
import Adapter.NewsListAdapter;
import Model.Document;
import Tools.Utils;

public class NewsList extends Fragment {

    ListView newsListView;
    String pageNumber,pageSize;
    private OnFragmentInteractionListener mListener;

    public NewsList() {
        // Required empty public constructor
    }


    public static NewsList newInstance() {
        NewsList fragment = new NewsList();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_news_list, container, false);
        newsListView = (ListView)v.findViewById(R.id.newsListView);
        newsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                mListener.loadNewsDetail(view.getTag().toString());

            }
        });
        loadNews();
        return  v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void loadNewsDetail(String newsObject);
    }

    void loadNews()
    {
        Ion.with(getContext())
                .load(Utils.Domain+"api/news/list")
                .setTimeout(30000)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        Gson gson = new Gson();
                        TypeToken<List<Document>> type = new TypeToken<List<Document>>() {
                        };
                        List<Document> articleList = gson.fromJson(result, type.getType());
                        if(articleList!=null && articleList.size()>0) {

                            NewsListAdapter _adapter = new NewsListAdapter(getContext(),articleList);
                            newsListView.setAdapter(_adapter);
                        }
                    }
                });
    }




}

package Modules;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
 
import com.example.hosein.Gym.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.StreamBody;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import java.io.File;
import java.util.List;

import Adapter.ArticleGroupAdapter;
import Adapter.ArticleListAdapter;
import Model.Document;
import Model.Group;
import Tools.Utils;

public class ArticlesList extends Fragment {

    ListView articlesListView;
    String groupId ,pageNumber,pageSize;
    private OnFragmentInteractionListener mListener;

    public ArticlesList() {
        // Required empty public constructor
    }


    public static ArticlesList newInstance(String groupId ) {
        ArticlesList fragment = new ArticlesList();
        Bundle args = new Bundle();
        args.putString("groupId",groupId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            groupId=getArguments().getString("groupId");
            Log.d("gym",groupId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_articles_list, container, false);
        articlesListView = (ListView)v.findViewById(R.id.articlesListView);
        articlesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String[] splitedPath;
                String filename="";
                String fileUrl = Utils.Domain + view.getTag().toString();
                if(fileUrl!=null && fileUrl.length()>1)
                {
                    splitedPath=fileUrl.split("/");

                    filename=splitedPath[splitedPath.length-1];
                }

              if(!checkIfExist(filename+".pdf")) {
                   downloadPdf(fileUrl,filename);
              }
                else
              {

                  Toast.makeText(getContext(),"from cash",Toast.LENGTH_LONG).show();
                  Intent intent = new Intent(Intent.ACTION_VIEW);
                  intent.setDataAndType(Uri.parse(Environment.getExternalStorageDirectory()+filename+".pdf"), "application/pdf");
                  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                  startActivity(intent);
              }
            }
        });
        loadArticleGroups();
        return  v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    void loadArticleGroups()
    {
        Ion.with(getContext())
                .load(Utils.Domain+"api/Article/List?groupId="+groupId+"&pageNumber=1&pageSize=15")
                .setTimeout(30000)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        Gson gson = new Gson();
                        TypeToken<List<Document>> type = new TypeToken<List<Document>>() {
                        };
                        List<Document> articleList = gson.fromJson(result, type.getType());
                        if(articleList!=null && articleList.size()>0) {

                            ArticleListAdapter _adapter = new ArticleListAdapter(getContext(),articleList);
                            articlesListView.setAdapter(_adapter);
                        }
                    }
                });
    }

    void downloadPdf(String fileUrl ,String filename)
    {


        Ion.with(getContext())
                .load(fileUrl)
                .write(new File(Environment.getExternalStorageDirectory(),filename+".pdf"))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {
                        Toast.makeText(getContext(),file.getAbsolutePath(),Toast.LENGTH_LONG).show();
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                        }
                        catch (Exception exp)
                        {
                           Toast.makeText(getContext()," faild to load pdf file",Toast.LENGTH_LONG).show();
                        }
                    }
                });





    }

    boolean  checkIfExist(String fileUrl)
    {
        File extStore = Environment.getExternalStorageDirectory();
        File myFile = new File(extStore.getAbsolutePath() + fileUrl+".pdf");
        if(myFile.exists()){
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(myFile), "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                return true;
            }
            catch (Exception exp)
            {
                Toast.makeText(getContext()," faild to load pdf file",Toast.LENGTH_LONG).show();
            }
        }
            return false;
    }
}
